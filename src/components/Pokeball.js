import { Container, Graphics, Sprite, Text } from "pixi.js";
import gsap from "gsap";

export default class Pokeball extends Container {
  constructor() {
    super();
    this.name = "pokeball";
    this.text = new Text();
    this.addChild(this.text);
    this.top = new Sprite.from("ballTop");
    this.addChild(this.top);
    this.bottom = new Sprite.from("bottom");
    this.addChild(this.bottom);
    this.isOpened = false;
    this.interactive = true;
    this.on(Pokeball.events.OPEN_START, () => {
      this.visible = false;
    });
  }

  static get events() {
    return {
      OPEN_END: "open_end",
      OPEN_START: "open_start",
      CLOSE_END: "close_end",
      CLOSE_START: "close_start",
    };
  }

  setRandomText() {
    if (this.text !== null) this.removeChild(this.text);
    var randomText = ["Pokemon", "Kiro", "Choko", "LioLio"];

    var num = Math.floor(Math.random() * randomText.length);
    if (num === 0) {
      num = 1;
    }
    this.text = new Text(randomText[num], {
      fontFamily: "Arial",
      fontSize: 100,
      fill: 0xffffff,
      align: "center",
    });
    this.text.position.x = 305;
    this.text.position.y = -140;
    this.text.anchor.set(0.5);
    this.addChild(this.text);
  }
  async _shuffle() {
    let prev = 0;

    const dummy = { value: 0 };
    const steps = gsap.to(dummy, {
      duration: 1,
      ease: "steps(100)",
      value: 100,
      paused: true,
      onUpdate: () => {
        if (dummy.value !== prev) this.setRandomText();
        prev = dummy.value;
      },
    });
    const steps2 = await gsap.to(steps, { duration: 5,progress: 1, ease: "circ.out" });
    console.log(steps2.progress())
    this.close();
    this.emit(Pokeball.events.CLOSE_START);
    this.removeChild(this.text);

  }

  open() {
    this.emit(Pokeball.events.OPEN_END)
    this.top.position.y = -495;
    this.bottom.position.y = 0;
    this._shuffle();
  }

  close() {
    this.transform.position.x = -350;
    this.top.transform.position.y = -350;
    this.bottom.transform.position.y = -108;
  }
}
