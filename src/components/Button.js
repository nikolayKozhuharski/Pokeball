import { Container, Graphics, Sprite } from "pixi.js";
import gsap from "gsap";
import Pokeball from "./Pokeball";

export default class Button extends Container {
  constructor() {
    super();
    this.name = "button";
    this.interactive = true;
    this.buttonMode = true;
    this.button = new Sprite.from("button");
    this.addChild(this.button);
    this.init();
  }

  init() {
    this.on("click", () => {
      this.emit(Pokeball.events.OPEN_START);
      this.hide()
    });
  }
  show() {
    this.alpha = 1;
    this.emit(Pokeball.events.CLOSE_END)

  }

  hide() {
    this.alpha = 0;
  }
}
