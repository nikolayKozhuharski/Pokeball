import { Sprite } from "pixi.js";
import Scene from "./Scene";
import gsap from "gsap";
import Footer from "../components/Footer";
import Pokeball from "../components/Pokeball";
import Button from "../components/Button";

export default class Play extends Scene {
  async onCreated() {
    const footer = new Footer();
    footer.x = -window.innerWidth / 2;
    footer.y = window.innerHeight / 2 - footer.height;
    this.addChild(footer);

    const pokeball = new Pokeball();
    pokeball.close();
    pokeball.scale.x = 1.5;
    pokeball.scale.y = 1.5;

    this.addChild(pokeball);

    const button = new Button();
    button.transform.scale.x = 1.5;
    button.transform.scale.y = 1.5;
    button.transform.position.x = pokeball.x / 2;
    button.transform.position.y = 220;

    this.addChild(button);

    button.on(Pokeball.events.OPEN_START, () => {
      pokeball.open();
    });
    pokeball.on(Pokeball.events.CLOSE_START, () => {
      button.show();
    });
  }

  /**
   * Hook called by the application when the browser window is resized.
   * Use this to re-arrange the game elements according to the window size
   *
   * @param  {Number} width  Window width
   * @param  {Number} height Window height
   */
  onResize(width, height) {
    // eslint-disable-line no-unused-vars
  }
}
